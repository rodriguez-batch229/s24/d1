/* 
    ES6 Updates 
    
    ES6 is one of the latest versions of writing JAVASCRIPT and in fact is one of the lates major update to JS

    let,const - are ES6 updates to update the standard of creating variables.
    var - was the keyword used previously before ES6

*/
// console.log(sampleLet);
// let sampleLet = "Sample";

// console.log(varSample);
// var varSample = "Hoist me Up!";

// Exponent operator

let fivePowerOf3 = Math.pow(5, 3);
console.log(fivePowerOf3);

// Math.pow() allows us to get the result of a number raised to the given exponent
// Math.pow(base, exponent)

// Exponent operator - ** - allows us to get the result of a number raised to a given exponent. it is used as an alternative to Math.pow()

let fivePowerOf2 = 5 ** 2;
console.log(fivePowerOf2);

let squareRootOf4 = 4 ** 0.5; //getting square root of a number
console.log(squareRootOf4);

// Template Literals
// "".'' => string literals

// how do we combine string?

let word1 = "Javascript",
    word2 = "Java",
    word3 = "is",
    word4 = "not";

// let sentence1 = word1 + " " + word2 + " " + word3 + " " + word4 + ".";
// console.log(sentence1);

// `` - back ticks - template literals - allows us to create strings usings `` and easily embed JS Expression

// ${} is used in template literals to embed JS Experssions and variables
// ${} - placeholder

let sentence1 = `${word1} ${word2} ${word3} ${word4}`;

console.log(sentence1);

let sentence2 = `${word2} is an OOP Language`;
console.log(sentence2);

// Template literals can also be used to embed JS Expression

let sentence3 = `The sum of 15 and 25 is ${15 + 25}`;
console.log(sentence3);

let user1 = {
    name: "Michael",
    position: "Manager",
    income: 90000,
    expenses: 50000,
};

console.log(`${user1.name} ${user1.position}`);

console.log(
    `His income is ${user1.income} and expenses at ${
        user1.expenses
    }. His current balance is ${user1.income - user1.expenses}`
);

// Destructuring Arrays and Objects
// Destructuring will allow us to save array elements or objects properties into new variables without having create/initialize with accessing items properties one by one

let array1 = ["Curry", "Lillard", "Paul", "Irving"];

// let player1 = array1[0]; //Curry
// let player2 = array1[1];
// let player3 = array1[2];
// let player4 = array1[3];
// console.log(player1, player2, player3, player4);

// Array Destructuring is when we save array items into variables
// In Arrays, Order matters and that goes the same for destructuring
let [player1, player2, player3, player4] = array1;

console.log(player1);
console.log(player4);

let array2 = ["Jokic", "Embiid", "Anthony-Towns", "Davis"];

let [center1, , , center2] = array2;
console.log(center1);

// Object Destructuring
// Object Destructuring allows us to get the value of a propert and save in a variable of the same name

let pokemon1 = {
    name: "Bulbasaur",
    type: "Grass",
    level: 10,
    moves: ["Razor Leaf", "Tackle", "Leach Seed"],
};

// Order does not matter in destructuring Object
// What matters are the keys/property name
// Variable name should be exactly match the property name
let { type, level, name, moves, personality } = pokemon1;

console.log(type);
console.log(level);
console.log(moves);
console.log(name);
console.log(personality); //undefined

//Destructuring in a function
function greet(object) {
    /* 
        object  = {
            name: "Michael",
            position: "Manager",
            income: 90000,
            expenses: 50000, 
        };  

    */
    let { name } = object; // allowed us to get object.name and save it as a name

    console.log(`Hello ${name}`);
    console.log(`${name} is my friend`);
    console.log(`Goodluck ${name}`);
}
greet(user1);

// Mini activity
// Destructure the following object and log the values of the name and price in the console

let product1 = {
    productName: "Safeguard Handsoap",
    description: "Liquid Handsoap by Safeguard",
    price: 25,
    isActive: true,
};

// Display the name and price in the console by destructuring.
// Send Screenshot of your output in the hangouts

function displayProduct(product) {
    let { productName, price } = product;

    console.log(`${productName}`);
    console.log(`${price}`);
}

displayProduct(product1);

// arrow functions
// Arrow functuions are an alternative way of writing functions in JS
// However, there significant pros and cons between traditional and arrow functions

// traditional function
function displayMessage(message) {
    console.log("Hello World!");
}

displayMessage();

// Arrow Function
const hello = () => {
    console.log(`Hello, Arrow!`);
};
hello();

//Arrow functions with parameters

const alertUser = (username) => {
    console.log(`This is an alert for user ${username}`);
};

alertUser("Manok");

// We dont usually use "let" keyword to assign our arrow function to avoid updating the variable
// alertUser = "Hello, o i sir • Rome";
// alertUser("romenick1");

// Arrow and traditional functions are pretty much the same. they are functions. However, there are some key differences

// Implicit return - is the ability of an arrow function to return value without the use of return keyword

// Traditional addNum function

function addNum(num1, num2) {
    let result = num1 + num2;
    return result;
}

console.log(addNum(5, 10));

// Arrow functions have implicit return. WHen an arrow function is written in one line, it can return value without return keyword and {}

const addNumArrow = (num1, num2) => num1 + num2;

console.log(addNumArrow(10, 20));

// If an arrow function is written in more than one line and with a {} then, we will need a return keyword
/* 
    const subNum = (num1, num2) => {
        return num1 - num2;
    }; 

*/
const subNum = (num1, num2) => num1 - num2;

console.log(subNum(20, 10));

// Traditional function vs arrow functions as object methods

let character1 = {
    name: "Cloud Strife",
    occupation: "SOLDIER",
    inntroduceName: function () {
        // In a traditional function as a method
        // This refers to the object where the method us
        console.log(`Hi! I am ${this.name}`);
    },
    introduceJob: () => {
        // In an arrow function as a method
        // This actually referes to the globa window object/the whole document
        // console.log(`My job is ${this.occupation}`);
        console.log(this);
    },
};

character1.inntroduceName();
character1.introduceJob();

const sampleObj = {
    name: "Sample1",
    age: 24,
};
sampleObj.name = "Smith";

console.log(sampleObj);

// Classes based objects blueprints
// In JavaScript, classes are templates of objects
// We can use classes to create objects following the structure of the class similar to a constructor

// Constructor Function
/* function Pokemon(name, type, level) {
        this.name = name;
        this.type = type;
        this.level = level;
    }

    let pokemonInstance1 = new Pokemon("Pikachu", "Electric", 25);

    console.log(pokemonInstance1); */

// With the advent of ES6, we are now introduce in a new way of creating objects with a blueprint with use of Classes.

class Car {
    constructor(brand, model, year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}

let car1 = new Car("Toyota", "Vios", "1991");
let car2 = new Car("Cooper", "Mini", "1967");
let car3 = new Car("Porsche", "911", "1968");

console.log(car1);
console.log(car2);
console.log(car3);

////Mini-Activity
// Translate the Pokemon constructor function into a Class Constructor
// Create 2 new pokemons out of the class constructor and save it in their variables
// Log the values in the console
// Share your output in the hangouts.

class Pokemon {
    constructor(name, type, level) {
        this.name = name;
        this.type = type;
        this.level = level;
    }
}
let pokemonInstance1 = new Pokemon("Pikachu", "Electric", 25);
let pokemonInstance2 = new Pokemon("Charizard", "Fire", 50);
let pokemonInstance3 = new Pokemon("Mewtwo", "Psych", 100);

console.log(pokemonInstance1);
console.log(pokemonInstance2);
console.log(pokemonInstance3);

// Arrow functions in array methods

let numArr = [2, 10, 3, 10, 5];

// Array methods traditional functions

/* let reduceNumber = numArr.reduce(function (x, y) {
    return x + y;
}); */

let reduceNumber = numArr.reduce((x, y) => x + y);

console.log(reduceNumber);

// Tip: If you are still getting confused on making arrow functions with array methods, first create the method with a traditional function, then translate that into arrow:

let mappedNum = numArr.map((num) => {
    return `${num} + RR`;
});

console.log(mappedNum);
